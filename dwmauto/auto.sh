#!/bin/sh

apt install figlet
figlet "hi !"

#input username
echo ">>> username :" 
read user 

#xorg install and other
apt install -y libxft-dev libfontconfig1 libx11-6 libxinerama-dev suckless-tools xorg x11-xserver-utils xserver-xorg-dev feh xfce4-terminal compton dmenu vim htop neofetch build-essential gcc

#install and enable lightdm
apt install -y lightdm
systemctl enable lightdm

#compton
mkdir /home/$user/.config/compton
cp /usr/share/doc/compton/examples/compton.sample.conf /home/.config/compton/compton.conf

#build dwm
cd suckless/dwm-6.2 && sudo make clean install
cd ../slstatus && sudo make clean install
cd ../..

#session lightdm
mkdir /usr/share/xsessions/
cp dwm.desktop /usr/share/xsessions/

#.xsession autostart
cp .xsession /home/$user/
chmod +x /home/$user/.xsession

#wallpaper
mkdir /home/Wallpaper
cp wallhaven-6odlvx_1600x900.png /home/Wallpaper

#done
echo "================== Installation DONE =================="
echo ">>>>> Restart now"
echo ">>>>> Run sudo reboot"
